import Logo from '../Logo';
import { NavLink } from 'react-router-dom';
import './Header.css';


export default function Header() {
	return (
		<header className="header">
			<Logo />

			<ul className="mainmenu">
				<li className="mainmenu-item">
					<NavLink to="/todo">Todo</NavLink>
				</li>
				<li className="mainmenu-item">
					<NavLink to="/users">Users</NavLink>
				</li>
			</ul>
		</header>
	);
}
