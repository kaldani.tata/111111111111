import { useContext } from 'react';
import { ThemeContext } from '../../contexts/ThemeContext';
import './ThemeButton.css';

export default function ThemeButton() {
	const { toggleTheme } = useContext(ThemeContext);

	return (
		<button className="theme-btn" onClick={toggleTheme}>
			Switch theme
		</button>
	);
}
