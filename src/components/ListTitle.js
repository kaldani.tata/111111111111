import React from 'react';

const ListTitle = () =>
	React.createElement(
		'h1',
		{
			style: {
				color: '#bbb',
			},
		},
		'Todo list'
	);

export default ListTitle;

