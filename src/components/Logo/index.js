import logo from './logo.svg';
import './Logo.css';

export default function Logo() {
	const classname = 'App-logo';

	return <img src={logo} className={classname} alt="logo" />;
}
