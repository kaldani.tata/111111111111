import React from 'react';
import './List.css';

function List({ data = [] }) {
	const toggleItem = (event) => {
		event.target.classList.toggle('done');
	};

	if (data.length === 0) return <p>No data</p>;

	return (
		<ul className="list">
			{data.map((item) => {
				return (
					<li onClick={toggleItem} className="list-item" key={item}>
						{item}
						<button className="delete-btn">x</button>
					</li>
				);
			})}
		</ul>
	);
}

export default List;
