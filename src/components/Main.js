import { UserList, User, Todo, UserDetails } from './../pages';
import { Routes, Route, Navigate } from 'react-router-dom';

export default function Main() {
	const style = { flexGrow: 1, padding: 20, display: 'flex', flexDirection: 'column', alignItems: 'center' };

	return (
		<main style={style}>
			<Routes>
				<Route path="/todo" element={<Todo />} />
				<Route path="/users" element={<UserList />}>
					{/* <Route path=":name" element={<UserDetails />} /> */}
				</Route>
				<Route path="/users/add" element={<User />} />
				<Route path="/users/:name" element={<UserDetails />} />
				<Route path="/" element={<Navigate to="/todo" />} />
				<Route path="*" element={<h1>Not found</h1>} />
			</Routes>
		</main>
	);
}
