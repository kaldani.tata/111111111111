import UserForm from './components/UserForm';
import './User.css';
import { useToggle } from '../../hooks/useToggle';
import { create } from './../../redux/reducers/userSlice';
import { useDispatch} from 'react-redux';
import { useNavigate } from 'react-router-dom';


export default function User() {
	const [showForm, toggleShowForm] = useToggle(true);
	const dispatch = useDispatch();
	const navigate = useNavigate();
	
	const handleFormSubmit = (data) => {
		dispatch(create(data));
		navigate({ pathname: '/users', search: `?name=${data.name}` });
	};

	return (
		<div className="user">
			<div className="heading">
				<h1>User form</h1>
				<button className="toggle-data-btn" onClick={toggleShowForm}>
					Toggle form
				</button>
			</div>

			{showForm && <UserForm onSubmit={handleFormSubmit} />}
		</div>
	);
}
