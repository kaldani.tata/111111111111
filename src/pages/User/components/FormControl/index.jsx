import './FormControl.css';

export default function FormControl({ label, children }) {
	return (
		<div className="form-control">
			<label>{label}</label>
			{children}
		</div>
	);
}
