import { useState, useEffect, Fragment } from 'react';

import './UserForm.css';
import FormControl from '../FormControl';
import { fetchTechnologies, fetchTopics, fetchSexes } from './../../../../redux/thunks/userThunk';
import { useDispatch, useSelector } from 'react-redux';

export default function UserForm({ onSubmit }) {
	const defaultData = {
		name: '',
		age: '',
		employed: false,
		sex: '',
		technology: 'react',
		topic: [],
		notes: '',
	};

	const [data, setData] = useState(defaultData);
	const [error, setError] = useState({
		name: false,
		age: false,
	});

	const [isValid, setIsValid] = useState(false);

	const dispatch = useDispatch();

	const { techList, sexList, topicList } = useSelector((state) => state.users);

	useEffect(() => {
		dispatch(fetchTechnologies());
		dispatch(fetchTopics());
		dispatch(fetchSexes());
	}, [dispatch]);

	const handleChange = (event) => {
		let value;

		switch (event.target.name) {
			case 'employed':
				value = event.target.checked;
				break;
			case 'topic':
				value = event.target.checked
					? [...data.topic, event.target.value]
					: data.topic.filter((item) => item !== event.target.value);
				break;
			default:
				value = event.target.value;
				break;
		}

		setData({ ...data, [event.target.name]: value });
	};

	const handleReset = () => {
		setData(defaultData);
	};

	const handleSubmit = (event) => {
		event.preventDefault();

		const errors = {};

		errors.name = data.name.length < 3;
		errors.age = data.age < 18;

		setError({ ...error, ...errors });

		if (!Object.values(errors).some(Boolean)) {
			onSubmit(data);
		}
	};

	useEffect(() => {
		setIsValid(() => {
			return !!(data.name && data.age);
		});
	}, [data]);

	return (
		<div className="user-form">
			<form onSubmit={handleSubmit}>
				<FormControl label="Name *">
					<input
						type="text"
						value={data.name}
						name="name"
						className={error.name ? 'error' : ''}
						onChange={handleChange}
					/>
				</FormControl>

				<FormControl label="Age *">
					<input
						type="number"
						value={data.age}
						name="age"
						className={error.age ? 'error' : ''}
						onChange={handleChange}
					/>
				</FormControl>

				<FormControl label="Employed">
					<input type="checkbox" name="employed" checked={data.employed} onChange={handleChange} />
				</FormControl>

				<FormControl label="Sex">
					<select name="sex" value={data.sex} onChange={handleChange}>
						<option value="">-- Please select --</option>
						{sexList.map((item) => (
							<option key={item.value} value={item.value}>
								{item.label}
							</option>
						))}
					</select>
				</FormControl>

				<FormControl label="Technology">
					{techList.map((item) => (
						<Fragment key={item.value}>
							<input
								type="radio"
								name="technology"
								onChange={handleChange}
								checked={item.value === data.technology}
								value={item.value}
							/>
							<span>{item.label}</span>
						</Fragment>
					))}
				</FormControl>

				<FormControl label="Topic">
					{topicList.map((item) => (
						<Fragment key={item.value}>
							<input
								type="checkbox"
								name="topic"
								onChange={handleChange}
								value={item.value}
								checked={data.topic.includes(item.value)}
							/>
							<span>{item.label}</span>
						</Fragment>
					))}
				</FormControl>

				<FormControl label="Age">
					<textarea value={data.notes} name="notes" onChange={handleChange} />
				</FormControl>

				<button type="submit" disabled={!isValid}>
					Save
				</button>
				<button type="reset" onClick={handleReset}>
					Reset
				</button>
			</form>

			<pre>{JSON.stringify(data, null, 4)}</pre>
		</div>
	);
}
