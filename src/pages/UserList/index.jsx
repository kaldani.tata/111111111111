import './UserList.css';
import { useSelector } from 'react-redux';
import { Link, Outlet } from 'react-router-dom';

export default function UserList() {
	const { list } = useSelector((state) => state.users);

	return (
		<div className="user-list">
			<div className="heading">
				<h1>user list</h1>
				<Link to="/users/add" className="add-btn">
					Add new user
				</Link>
			</div>
			<table className="table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Age</th>
						<th>Employed</th>
						<th>Technologies</th>
						<th>Topics</th>
						<th>Notes</th>
					</tr>
				</thead>
				<tbody>
					{list.map((user, index) => (
						<tr key={index}>
							<td>
								<Link to={`/users/${user.name}`}>{user.name}</Link>
							</td>
							<td>{user.age}</td>
							<td>{user.employed ? 'Yes' : 'No'}</td>
							<td>{user.technology}</td>
							<td>{user.topic.join(', ')}</td>
							<td>{user.notes.substring(0, 100)}</td>
						</tr>
					))}
				</tbody>
			</table>

			<Outlet />
		</div>
	);
}
