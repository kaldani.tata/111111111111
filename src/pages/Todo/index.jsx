import { List, ListTitle } from './../../components';
import TodoForm from './components/TodoForm';
import { useDispatch, useSelector } from 'react-redux';
import { create } from './../../redux/reducers/todoSlice';

export default function Todo() {
	const dispatch = useDispatch();
	const { list } = useSelector((state) => state.todo);

	const addTask = (title) => {
		const isDuplicate = list.some((item) => item === title);
		if (isDuplicate) return;

		dispatch(create(title));
	};

	return (
		<>
			<TodoForm onSubmit={addTask} />

			<ListTitle />
			<List data={list} />
		</>
	);
}
