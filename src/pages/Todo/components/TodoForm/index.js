import './TodoForm.css';
import { useState } from 'react';

export default function TodoForm({ onSubmit }) {
	const [textField, setTextField] = useState('');

	const handleChange = (event) => {
		setTextField(event.target.value);
	};

	const handleSubmit = (event) => {
		event.preventDefault();

		onSubmit(textField);
		setTextField('');
	};

	return (
		<form className="todo-form" onSubmit={handleSubmit}>
			<input type="text" placeholder="Type here..." className="text-field" value={textField} onChange={handleChange} />
		</form>
	);
}
