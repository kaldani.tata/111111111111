import React from 'react';
import './List.css';

function List({ data = [], onChangeStatus }) {
	const toggleItem = (title) => {
		onChangeStatus(title);
	};

	if (data.length === 0) return <p>No data</p>;

	return (
		<ul className="list">
			{data.map((item) => {
				return (
					<li onClick={() => toggleItem(item)} className="list-item" key={item}>
						{item}
						<button className="delete-btn">x</button>
					</li>
				);
			})}
		</ul>
	);
}

export default List;

