import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';

export default function UserDetails() {
	const params = useParams();

	const user = useSelector((state) => state.users.list.find((user) => user.name === params.name));

	if (!user) return <h1>User not found</h1>;

	return (
		<div>
			<h1>User</h1>
			<ul>
				<li>
					{user.name} {user.age} y.o.
				</li>
				<li>
					{user.technology}: {user.topic.join(', ')}
				</li>
			</ul>
		</div>
	);
}
