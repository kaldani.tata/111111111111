import './App.css';
import { useState } from 'react';
import { Header, Footer, Main } from './components';
import ThemeButton from './components/ThemeButton';
import { ThemeContext } from './contexts/ThemeContext';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import { BrowserRouter } from 'react-router-dom';



function App() {
	const [theme, setTheme] = useState('light');

	const toggleTheme = () => {
		setTheme(theme === 'light' ? 'dark' : 'light');
	};

	return (
		<BrowserRouter>
		<Provider store={store}>
			<ThemeContext.Provider value={{ theme, toggleTheme }}>			
			<div className={`App ${theme}`}>
						<ThemeButton />
						<Header />
						<Main />
						<Footer />
					</div>
		</ThemeContext.Provider>
		</Provider>
		</BrowserRouter>		
	);
}

export default App;

