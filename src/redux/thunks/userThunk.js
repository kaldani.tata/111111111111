import { technologiesLoaded, sexLoaded, topicsLoaded } from '../reducers/userSlice';

export const fetchTechnologies = () => async (dispatch) => {
	const data = await fetch('data/techList.json').then((response) => response.json());

	dispatch(technologiesLoaded(data));
};

export const fetchSexes = () => async (dispatch) => {
	const data = await fetch('data/sexList.json').then((response) => response.json());

	dispatch(sexLoaded(data));
};

export const fetchTopics = () => async (dispatch) => {
	const data = await fetch('data/topicList.json').then((response) => response.json());

	dispatch(topicsLoaded(data));
};

