import { configureStore } from '@reduxjs/toolkit';
import userReducer from './reducers/userSlice';
import todoSlice from './reducers/todoSlice';



export const store = configureStore({
	reducer: {
		users: userReducer,
		todo: todoSlice,
	},
});

