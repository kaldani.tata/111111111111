import { createSlice } from '@reduxjs/toolkit';

const todoSlice = createSlice({
	name: 'todo',
	initialState: {
		list: [],
	},
	reducers: {
		create: (state, action) => {
			state.list.push(action.payload);
		},
	},
});

export const { create } = todoSlice.actions;

export default todoSlice.reducer;

