import { createSlice } from '@reduxjs/toolkit';

const userSlice = createSlice({
	name: 'user',
	initialState: {
		list: [],
		techList: [],
		sexList: [],
		topicList: [],
	},
	reducers: {
		create: (state, action) => {
			state.list.push(action.payload);
		},
		technologiesLoaded: (state, action) => {
			state.techList = action.payload;
		},
		sexLoaded: (state, action) => {
			state.sexList = action.payload;
		},
		topicsLoaded: (state, action) => {
			state.topicList = action.payload;
		},
	},
});

export const { create, technologiesLoaded, sexLoaded, topicsLoaded } = userSlice.actions;

export default userSlice.reducer;

